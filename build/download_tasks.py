import os
import tarfile
import pathlib

from invoke import task, Exit
from build.utils import info, warning, error, yippee
from build.utils import mkdir, verify_exists

def download_and_unpack(url, destination, filename):
    os.system(f"wget -q --show-progress --progress=bar:force:noscroll {url} -O{destination}{filename}")
    info("unpacking...")
    os.system(f"tar -C {destination} -xf {destination}{filename}")
    with tarfile.open(f"{destination}{filename}", "r") as tar:
        tar_members = tar.getmembers()
        folder_base = None
        for member in tar_members:
            if member.type == tarfile.DIRTYPE:
                folder_base = member.name
                break
        if not folder_base: # last resort, for binutils etc
            path = pathlib.Path(tar_members[0].name)
            folder_base = path.parts[0]
            warning(f"last resort folder base guess: {folder_base}?")
    os.system(f"rm {destination}{filename}")
    os.rename(f"{destination}{folder_base}", f"{destination}{filename}")

def download_git(repo, destination, name):
    os.system(f"cd {destination} ; git clone --depth=1 {repo} {name}")

all_download_tasks = []
all_clone_tasks = []

class DownloadTask(object):
    def __init__(self, package_name):
        self.package_name = package_name
    
    def __call__(self, c):
        if verify_exists(os.path.join(c.extconfig["SOURCE_DIR"], self.package_name)): return
        if f"{self.package_name.upper()}_URL" not in c.extconfig:
            error(f"URL not found in config: {self.package_name.upper()}_URL")
            return
        info(f"downloading and unpacking {self.package_name}")
        download_and_unpack(c.extconfig[f"{self.package_name.upper()}_URL"], c.extconfig["SOURCE_DIR"], self.package_name)
        yippee("done!")

class CloneTask(object):
    def __init__(self, package_name):
        self.package_name = package_name
    
    def __call__(self, c):
        if verify_exists(os.path.join(c.extconfig["SOURCE_DIR"], self.package_name)): return
        if f"{self.package_name.upper()}_REPO" not in c.extconfig:
            error(f"Repo not found in config: {self.package_name.upper()}_REPO")
            return
        info(f"cloning {self.package_name}")
        download_git(c.extconfig[f"{self.package_name.upper()}_REPO"], c.extconfig["SOURCE_DIR"], self.package_name)
        yippee("done!")

def dl(names):
    global all_download_tasks
    for name in names:
        all_download_tasks.append(task(DownloadTask(name), name=name))

def clone(names):
    global all_clone_tasks
    for name in names:
        all_clone_tasks.append(task(CloneTask(name), name=name))

dl([
    "gcc", "binutils", "flex", "zlib", "bzip2", "bash", "coreutils", "e2fsprogs",
    "pkgconfiglite", "libuuid", "util_linux", "grub", "shadow", "gmp", "mpfr",
    "mpc", "gdb", "libxcrypt", "ncurses", "vim", "gpg_error", "gcrypt", 
    "dmidecode", "parted", "make", "grep", "gawk", "sed", "dash", "libpciaccess"
])

clone([
    "gnumach", "mig", "hurd", "rumpkernel", "libacpica", "glibc"
])

@task
def create_source_dir(c):
    if verify_exists(c.extconfig["SOURCE_DIR"]): return
    mkdir(c.extconfig["SOURCE_DIR"])

@task(pre=[create_source_dir] + all_download_tasks + all_clone_tasks)
def download_all(c):
    yippee("refreshed all sources successfully!")
