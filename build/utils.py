import glob
import os
import invoke

COLOUR_NORMAL = "\x1b[39;49;1;4m"
COLOUR_WARNING = "\x1b[33;49;1;4m"
COLOUR_ERROR = "\x1b[31;49;1;4m"
COLOUR_YIPPEE = "\x1b[32;49;1;4m"
COLOUR_RESET = "\x1b[0m"

def verify_exists(wildcard):
    return (len(glob.glob(wildcard)) > 0)

def info(message):
    print(f"» {COLOUR_NORMAL}{message}{COLOUR_RESET}")

def warning(message):
    print(f"! {COLOUR_WARNING}{message}{COLOUR_RESET}")

def error(message):
    raise invoke.Exit(f"!! {COLOUR_ERROR}{message}{COLOUR_RESET}")

def yippee(message):
    print(f"😃 {COLOUR_YIPPEE}{message}{COLOUR_RESET}")

def mkdir(directory):
    info(f"creating {directory}")
    os.mkdir(directory)
