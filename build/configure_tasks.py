import os
import glob
from invoke import task, Exit
from build.utils import info, warning, error, yippee
from build.utils import mkdir, verify_exists
from build.download_tasks import all_download_tasks, all_clone_tasks
from build.patch_tasks import all_patch_tasks
from build import commands

all_configure_tasks = []

class ConfigureTask(object):
    def __init__(self, package_name):
        self.package_name = package_name
    
    def __call__(self, c):
        info(f"configuring {self.package_name}")
        commands.configure(c.extconfig["SOURCE_DIR"], 
            self.package_name, 
            c.package_config[self.package_name]["configure"],
            c.package_config[self.package_name]["configure_env_ephemeral"],
            c.build_vars)
        yippee("done!")

def configure(names):
    global all_configure_tasks
    for name in names:
        all_configure_tasks.append(task(ConfigureTask(name), name=name))

configure(["binutils"])

@task(pre=all_download_tasks + all_clone_tasks + all_patch_tasks)
def configure_all(c):
    yippee("configured all sources successfully!")
