import os

def configure(source_dir, package, options, ephemeral_vars, build_vars):
    options_unpacked = ""
    for option in options.keys():
        if options[option]:
            options_unpacked += f"--{option}={options[option]} "
        else:
            options_unpacked += f"--{option} "
    options_unpacked = options_unpacked.format_map(build_vars)  # resolve things like {HOST} for example
    ephemeral_vars_unpacked = ""
    for var in ephemeral_vars.keys():
        ephemeral_vars_unpacked += f"{var}={ephemeral_vars[var]} "
    ephemeral_vars_unpacked = ephemeral_vars_unpacked.format_map(build_vars)
    os.system(f"{ephemeral_vars_unpacked}{source_dir}/{package}/configure {options_unpacked}")
