import os
import glob
from invoke import task, Exit
from build.utils import info, warning, error, yippee
from build.utils import mkdir, verify_exists
from build.download_tasks import all_download_tasks, all_clone_tasks

def apply_all_patches(base_dir, source_dir, package_name):
    patch_list = glob.glob(f"patches/{package_name}/*.*")
    for patch in patch_list:
        print(patch)
        result_raw = os.system(f"cd {source_dir}/{package_name} ; patch -f -Np1 --dry-run < {base_dir}/{patch} > /dev/null 2>&1")
        result = os.WEXITSTATUS(result_raw)
        if not result:
            os.system(f"cd {source_dir}/{package_name} ; patch -Np1 < {base_dir}/{patch}")

all_patch_tasks = []

class PatchTask(object):
    def __init__(self, package_name):
        self.package_name = package_name
    
    def __call__(self, c):
        info(f"patching {self.package_name}")
        apply_all_patches(c.base_dir, c.extconfig["SOURCE_DIR"], self.package_name)
        yippee("done!")

def patch(names):
    global all_patch_tasks
    for name in names:
        all_patch_tasks.append(task(PatchTask(name), name=name))

patch(["gnumach", "hurd", "glibc", "gcc", "gpg_error", "grep", "dash", "grub"])

@task(pre=all_patch_tasks)
def patch_all(c):
    yippee("patched all sources successfully!")
