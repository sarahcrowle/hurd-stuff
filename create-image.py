#!/usr/bin/env python3

# TODO: better error handling
# TODO: we need a step still that gets grub going and stitches the 
#   partition table and fs image together... almost there!
#   if we can't get grub-install to work, we could just have a copy
#   of grub laying around that we inject into the fs image. I think
#   that'll *probably* work. probably?

import os
import os.path
import shutil
import sys
from pathlib import Path
import datetime

# replace stdout so we don't have buffering... we don't need it
#  and it makes status messages clearer
class Unbuffered(object):
   def __init__(self, stream):
       self.stream = stream
   def write(self, data):
       self.stream.write(data)
       self.stream.flush()
   def writelines(self, datas):
       self.stream.writelines(datas)
       self.stream.flush()
   def __getattr__(self, attr):
       return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

KB = 1000
MB = 1000 * KB
GB = 1000 * MB

KiB = 1024
MiB = 1024 * KiB
GiB = 1024 * MiB

IMG_NAME = "hurd.img"
BASE_SYSROOT = "tools-i686" # TODO: make this nicer
FS_START = 2048
SECTOR_SIZE = 512

# TODO: actual arg handling
SYSTEM = sys.argv[1]

def alloc_file(file_name, size):
    with open(file_name, "wb") as f:
        f.write(b"\x00" * size)

def truncate_file(file_name, size):
    with open(file_name, "ab") as f:
        f.truncate(size)

def mk_all_dirs(basepath, names):
    for name in names:
        print(f"    MK! {os.path.join(basepath, name)}")
        os.mkdir(os.path.join(basepath, name))

def touch_all(basepath, names):
    for name in names:
        print(f"    TOUCH! {os.path.join(basepath, name)}")
        Path(os.path.join(basepath, name)).touch()

def copy_all_trees(basepath, dest_basepath, names):
    for name in names:
        print(f"    COPYTREE! {os.path.join(basepath, name)} > {dest_basepath}/")
        shutil.copytree(os.path.join(basepath, name), os.path.join(dest_basepath, name), dirs_exist_ok=True)

def prepare():
    print("  preparing blank images...")

    print("    partition_table.img...", end="")
    if not os.path.isfile("partition_table.img"):
        alloc_file("partition_table.img", 10 * GiB)
        os.system("parted -a optimal -s partition_table.img mklabel msdos")
        os.system("parted -a optimal -s partition_table.img -- mkpart primary ext2 2048s -1")
        os.system("parted -a optimal -s partition_table.img -- set 1 boot on")
        truncate_file("partition_table.img", FS_START * SECTOR_SIZE)
        print("done!")
    else:
        print("not necessary :D")

    print ("    ext2-blank.img...", end="")
    if not os.path.isfile("ext2-blank.img"):
        # seems magical... it's just the total sectors in the blank
        #  table we just made, offset by the fs start, 
        #  times the sector size
        alloc_file("ext2-blank.img", (20967424 - FS_START) * SECTOR_SIZE)
        print("done!")
    else:
        print("not necessary :D")

    print("  compiling Ariadne's cat if necessary...")
    if not os.path.isfile("ariadne-cat"):
        os.system("gcc ariadne-cat.c -oariadne-cat")
        print("  done!")
    else:
        print("  not necessary! :D")

def copy():
    # FIXME: this can fail silently because we don't have permission
    #   (and in fact it often does). make a wrapper that escalates 
    #   if necessary
    print ("  deleting old tree (THIS MAY FAIL SILENTLY)...", end="")
    shutil.rmtree("new-sysroot", ignore_errors=True)
    print ("done!")

    print ("  creating FHS directories...")
    os.mkdir("new-sysroot")
    mk_all_dirs("new-sysroot", 
        ["etc", 
         "boot",
         "dev",
         "usr",
         "hurd",
         "servers",
         "lib",
         "libexec",
         "proc",
         "sbin",
         "bin",
         "var",
         "root",
         "share"])
    mk_all_dirs("new-sysroot/var", ["run", "lib"])
    mk_all_dirs("new-sysroot/servers", ["socket", "bus"])
    print("  done!")

    print ("  copying /etc...", end="")
    shutil.copytree("files/etc", "new-sysroot/etc", dirs_exist_ok=True)
    os.mkdir("new-sysroot/etc/hurd")
    print("done!")

    print ("  copying runsystem.hurd...", end="")
    shutil.copy("files/runsystem.hurd", "new-sysroot/libexec/")
    print("done!")

    # TODO: use os.chmod. I'm lazy tho
    os.system("chmod ogu+x new-sysroot/libexec/runsystem.hurd")

    print("  copying grub.cfg and gnumach...", end="")
    os.mkdir("new-sysroot/boot/grub2")
    shutil.copy("files/boot/grub.cfg", "new-sysroot/boot/grub2")
    shutil.copy(os.path.join(SYSTEM, "boot/gnumach.gz"), "new-sysroot/boot")
    print("done!")
    
    print("  copying grub2 modules...", end="")
    os.mkdir("new-sysroot/boot/grub2/i386-pc")
    shutil.copytree(f"{SYSTEM}/{BASE_SYSROOT}/lib/grub2/i386-pc", "new-sysroot/boot/grub2/i386-pc", dirs_exist_ok=True)
    print("done!")

    print("  touching servers...")
    touch_all("new-sysroot/servers",
        ["acpi",
         "exec",
         "crash-kill",
         "default-pager",
         "password",
         "socket",
         "startup",
         "proc",
         "auth",
         "symlink"])
    print("  done!")
    
    os.mkdir("new-sysroot/tmp")
    os.chmod("new-sysroot/tmp", 0o1777)

    print("  copying trees...")
    copy_all_trees(os.path.join(SYSTEM, BASE_SYSROOT), "new-sysroot",
        ["hurd",
         "dev",
         "sbin",
         "bin",
         "lib",
         "share",
         "etc",
         "libexec"])
    print("  done!")

    shutil.copy("files/rc", "new-sysroot/libexec")
    shutil.copy("files/runsystem", "new-sysroot/libexec")

    print("  setting up ld.so...")
    if os.path.isfile("new-sysroot/lib/ld-x86_64.so.1"):
        # TODO: use os.symlink. again, I'm lazy
        os.system("cd new-sysroot/lib ; ln -sfv ld-x86_64.so.1 ld.so")
    else:
        os.system("cd new-sysroot/lib ; ln -sfv ld.so.1 ld.so")
    print("  done!")
    
    # TODO: ditto
    print("  setting up /bin/sh...")
    os.system(f"ln -svf / new-sysroot/{BASE_SYSROOT}")
    os.system("ln -svf /bin/bash new-sysroot/bin/sh")
    print("  done!")

    shutil.copy("files/SETUP", "new-sysroot")

    # TODO: the original script writes out a manifest here. 
    #  it's a good idea, but I don't really care right now.

    print("  creating motd...", end="")
    date_string = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
    current_user = os.environ.get("USER")
    with open("new-sysroot/etc/motd", "w") as motd_f:
        motd_f.write("Welcome to the HURD!\n")
        motd_f.write(f"Compiled {date_string} by {current_user}")
    print("done!")
    
    # TODO: os.chown, but I'm lazy (I say that a lot)
    # TODO: also, we shouldn't REQUIRE sudo
    print("  changing ownership of the rootfs (REQUIRES SUDO)...")
    os.system("sudo chown -R root:root new-sysroot")
    print("  done!")

def write_to_fs_image():
    print("  copying blank fs image...", end="")
    shutil.copy("ext2-blank.img", "fs.img")
    print("done!")
    
    print("  mke2fs -T hurd -m 1 -t ext2 -b 4096 -F -q -d new-sysroot -o hurd fs.img...")
    os.system("MKE2FS_CONFIG=new-sysroot/etc/mke2fs.conf mke2fs -T hurd -m 1 -t ext2 -b 4096 -F -q -d new-sysroot -o hurd fs.img")
    print("  done!")

def stitch_image():
    print("  stitching image with Ariadne's cat...", end="")
    os.system(f"./ariadne-cat partition_table.img fs.img > {IMG_NAME}.noboot")
    print("done!")

def apply_grub_patch():
    print("  applying grub patch...")
    print("    ripping out the first 2M of the image...")
    os.system(f"dd if={IMG_NAME}.noboot of={IMG_NAME}.head bs=2M count=1")
    print("    making a hexdump...", end="")
    os.system(f"xxd -c1 {IMG_NAME}.head > {IMG_NAME}.head.hex")
    print("done!")
    print("    patching that hexdump...")
    os.system(f"patch --fuzz=10 {IMG_NAME}.head.hex hurd-grub.patch")
    print("    making it binary again...", end="")
    os.system(f"xxd -c1 -r {IMG_NAME}.head.hex > {IMG_NAME}.head")
    print("done!")
    print("    shoving it back into the image...")
    os.system(f"dd if={IMG_NAME}.head of={IMG_NAME}.noboot conv=notrunc")
    print("    renaming...")
    os.rename(f"{IMG_NAME}.noboot", f"{IMG_NAME}")
    print("  done!")

if __name__ == "__main__":
    print("PREPARE")
    prepare()
    print("COPY")
    copy()
    print("WRITE TO FS IMAGE")
    write_to_fs_image()
    print("STITCH IMAGE")
    stitch_image()
    print("APPLY GRUB PATCH")
    apply_grub_patch()
