from invoke import Collection, task
from build import download_tasks, patch_tasks, configure_tasks
import json
import os

@task(pre=[download_tasks.download_all, patch_tasks.patch_all, configure_tasks.configure_all], name="all")
def build_all(c):
    print("built successfully :3")

ns = Collection()

build = Collection("build")
build.add_task(build_all)
ns.add_collection(build)

download = Collection("download")
for download_or_clone_task in download_tasks.all_download_tasks + download_tasks.all_clone_tasks:
    download.add_task(download_or_clone_task)

patch = Collection("patch")
for patch_task in patch_tasks.all_patch_tasks:
    patch.add_task(patch_task)

configure = Collection("configure")
for configure_task in configure_tasks.all_configure_tasks:
    configure.add_task(configure_task)

ns.add_collection(download)
ns.add_collection(patch)
ns.add_collection(configure)

with open("build_config.json", "r") as config: 
    ns.configure({"extconfig": json.load(config)})

with open("build_vars.json", "r") as build_vars_f:
    build_vars = json.load(build_vars_f)
    for build_var in build_vars.keys():
        # TODO: support more substitutions than just PWD
        build_vars[build_var] = build_vars[build_var].format(PWD=os.getcwd())
    ns.configure({"build_vars": build_vars})

with open("package_config.json", "r") as package_config:
    ns.configure({"package_config": json.load(package_config)})

ns.configure({"base_dir": os.getcwd()})
