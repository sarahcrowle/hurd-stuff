#!/bin/bash
while true; do
dialog --title "Stop qemu" --ok-label "Yes please" --msgbox "Stop qemu?" 5 15
killall qemu-system-i386
done
