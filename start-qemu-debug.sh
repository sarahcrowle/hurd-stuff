#!/bin/sh

qemu-system-i386 -drive file=$1,format=raw -m 4096 -monitor unix:qemu-monitor-socket,server,nowait -display curses
